{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE FlexibleInstances #-}

module Server
    (buildApp)
where

import           Components.EntityStore.SQLite
import           Control.Exception (Exception)
import           Control.Monad.IO.Class (liftIO)
import           Control.Monad.Trans.Reader (ask)
import           Data.Aeson (ToJSON(..))
import           Data.Maybe (isJust, fromJust)
import           Data.Proxy
import           Data.Semigroup ((<>))
import           Data.UUID (UUID)
import           Database.SQLite.Simple (FromRow)
import           Servant.API
import           Servant (throwError)
import           Servant.Server (ServerT, Application, ServantErr(..), serve, hoistServer, err500, err400)
import           Types
import qualified Data.ByteString.Lazy.Char8 as DBL8
import qualified Data.Text as DT


type Name = DT.Text
type Greeting = DT.Text

type TodoAPI =
    "todos" :> Get '[JSON] [WithUUID Task]
    :<|> "todos" :> Capture "uuid" UUID :> Get '[JSON] (WithUUID Task)
    :<|> "todos" :> Capture "uuid" UUID :> ReqBody '[JSON] (Partial TaskF) :> Patch '[JSON] (WithUUID Task)
    :<|> "todos" :> Capture "uuid" UUID :> Delete '[JSON] (WithUUID Task)
    :<|> "todos" :> ReqBody '[JSON] Task :> Post '[JSON] (WithUUID Task)

todoServer :: ServerT TodoAPI AppHandler
todoServer = listTodos
             :<|> getTodoByUUID
             :<|> patchTodoByUUID
             :<|> deleteTodoByUUID
             :<|> createTodo

listTodos :: AppHandler [WithUUID Task]
listTodos = ask
            >>= \(AppState _ estore) -> liftIO (list estore :: IO (Either EntityStoreError [WithUUID Task]))
            >>= rightOrServantErr genericServerError

getTodoByUUID :: UUID -> AppHandler (WithUUID Task)
getTodoByUUID uuid = ask
                     >>= \(AppState _ estore) -> liftIO (getByUUID estore uuid :: IO (Either EntityStoreError (WithUUID Task)))
                     >>= rightOrServantErr genericServerError

patchTodoByUUID :: UUID -> Partial TaskF -> AppHandler (WithUUID Task)
patchTodoByUUID uuid partial = pure (validate partial)
                               >>= rightOrConvertToServantErr
                               >>= \validated -> ask
                               >>= \(AppState _ estore) -> liftIO (updateByUUID estore uuid validated :: IO (Either EntityStoreError (WithUUID (Complete TaskF))))
                               >>= rightOrConvertToServantErr
                               >>= pure . (toTaskFromF <$>)

deleteTodoByUUID :: UUID -> AppHandler (WithUUID Task)
deleteTodoByUUID uuid = ask
                        >>= \(AppState _ estore) -> liftIO (deleteByUUID estore uuid :: IO (Either EntityStoreError (WithUUID Task)))
                        >>= rightOrServantErr genericServerError

createTodo :: Task -> AppHandler (WithUUID Task)
createTodo todo = pure (validate todo)
                  >>= rightOrConvertToServantErr
                  >>= \validated -> ask
                  >>= \(AppState _ estore) -> liftIO (create estore validated :: IO (Either EntityStoreError (WithUUID Task)))
                  >>= rightOrConvertToServantErr

todoAPI :: Proxy TodoAPI
todoAPI = Proxy

buildApp :: AppState -> Application
buildApp state = serve todoAPI $ hoistServer todoAPI naturalTransform todoServer
    where
      naturalTransform = appToServantHandler state

-- | Ensure that an Either resolves to it's Right value
rightOrServantErr :: Exception a => ServantErr -> Either a b -> AppHandler b
rightOrServantErr err (Left _)  = throwError err
rightOrServantErr _   (Right v) = return v

-- | Ensure that an Either resolves to it's Right value
rightOrConvertToServantErr :: (Exception err, ServableError err) => Either err b -> AppHandler b
rightOrConvertToServantErr (Left err)  = throwError $ toServantError err
rightOrConvertToServantErr (Right v) = return v

genericServerError :: ServantErr
genericServerError = err500 { errBody = "Unexpected server error" }

makeValidationErr :: [ValidationError] -> ServantErr
makeValidationErr verrs = err400 { errBody = "Validation errors occurred:\n " <> DBL8.pack (show verrs)  }

class ServableError err where
    toServantError :: err -> ServantErr

instance ServableError EntityStoreError where
    toServantError (UnexpectedErrorES txt) = err500 { errBody = DBL8.pack (DT.unpack txt) }
    toServantError _ = err500 { errBody = "Unexpected error" }

instance ServableError [ValidationError] where
    toServantError errs = makeValidationErr errs
