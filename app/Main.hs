{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import Components.EntityStore.SQLite
import Config (AppConfig(..), EntityStoreConfig, Host, Port, ProcessEnvironment(..), makeAppConfig)
import Control.Monad (join)
import Data.Functor.Identity
import Data.Semigroup ((<>))
import Lib
import Network.Wai.Handler.Warp (run)
import Options.Applicative
import Server (buildApp)
import System.Environment (getEnvironment)
import Text.Pretty.Simple (pPrint)
import Types
import Util (rightOrThrow)

data Options = Options
    { cfgPath :: Maybe FilePath
    , cmd :: Command
    }

data Command = Serve
             | ShowConfig deriving (Eq)

-- | Parser for commands
parseCommands :: Parser Command
parseCommands = subparser commands
    where
      serverCmd :: ParserInfo Command
      serverCmd = info (pure Serve) (progDesc "Start the server")

      showConfigCmd :: ParserInfo Command
      showConfigCmd = info (pure ShowConfig) (progDesc "Show configuration")

      commands :: Mod CommandFields Command
      commands = command "server" serverCmd
                 <> command "show-config" showConfigCmd

-- | Parser for top level options
parseOptions :: Parser (Maybe FilePath)
parseOptions = optional
               $ strOption ( long "config"
                           <> short 'c'
                           <> metavar "FILENAME"
                           <> help "Configuration file (.json/.toml)" )

-- | Top level optparse-applicative parser for the entire CLI
parseCmdLine :: Parser Options
parseCmdLine = Options <$> parseOptions <*> parseCommands


-- | Helper function to access the environment and marshall it into our newtype
pullEnvironment :: IO ProcessEnvironment
pullEnvironment = ProcessEnvironment <$> getEnvironment

-- | IO action that shows the current loaded configuration
showConfig :: Options -> IO ()
showConfig Options{cfgPath=path} = pullEnvironment
                                   >>= makeAppConfig path
                                   >>= pPrint

-- | IO action that runs the server
runServer :: Options -> IO ()
runServer Options{cfgPath=path} = pullEnvironment
                                  >>= makeAppConfig path
                                  >>= rightOrThrow
                                  >>= server

-- | Build an entity store for use in the main application
buildEntityStore :: Complete EntityStoreConfig -> IO SQLiteEntityStore
buildEntityStore cfg = putStrLn "[info] initializing EntityStore with config:"
                       >> pPrint cfg
                       >> (construct cfg :: IO (Either EntityStoreError SQLiteEntityStore))
                       >>= rightOrThrow

-- | Start up the server and serve requests
server :: Complete AppConfig -> IO ()
server cfg = buildEntityStore entityStoreCfg -- | Build & start the entity store
             >>= \entityStore -> start entityStore
             -- | TEST CODE, REMOVE
             >> makeTestTask
             >>= rightOrThrow
             >>= create entityStore
             >>= rightOrThrow
             -- | Build the app config with the entity store
             >> pure (AppState cfg entityStore)
             -- | Start the app
             >>= startApp
    where
      makeTestTask = pure $ validate $ NotStartedT (Identity "test") (Identity "test description")

      entityStoreCfg = runIdentity $ entityStoreConfig cfg
      appPort = runIdentity $ port cfg

      startApp state = putStrLn ("Starting server at port [" <> show appPort <> "]...")
                       >> run appPort (buildApp state)

main :: IO ()
main = parseOptions
       >>= process
    where
      cmdParser :: ParserInfo Options
      cmdParser = info parseCmdLine idm

      parseOptions :: IO Options
      parseOptions = execParser cmdParser

      process :: Options -> IO ()
      process opts = case cmd opts of
                       Serve -> runServer opts
                       ShowConfig -> showConfig opts
