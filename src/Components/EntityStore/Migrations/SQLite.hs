{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module Components.EntityStore.Migrations.SQLite where

import NeatInterpolation (text)
import Types (SQLMigration(..), SQLMigrationQuery(..))

migrations :: [SQLMigration]
migrations =
    [SQLMigration
     { smFrom=0
     , smTo=1
     , smQuery=
         SQLMigrationQuery
         [text|
               CREATE TABLE tasks(
                 uuid TEXT PRIMARY KEY NOT NULL,
                 name TEXT NOT NULL,
                 description TEXT NOT NULL,
                 state TEXT NOT NULL
               );
               |]
     }
    ]
