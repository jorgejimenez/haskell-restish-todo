{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StandaloneDeriving #-}

module Types where

import           Data.HashMap.Lazy (insert)
import           GHC.Generics (Generic)
import           Data.Aeson (FromJSON(..), ToJSON(..), (.=), Value(..), object)
import           Control.Monad.Trans.Reader (ReaderT(..))
import           Data.Bifunctor (second)
import           Data.Kind(Type, Constraint)
import           Config (AppConfig, CompleteTaskStoreConfig)
import           Control.Exception (throw, Exception)
import           Data.Either (isRight)
import           Data.Functor.Identity (Identity(..))
import           Data.Int (Int64)
import           Data.List (uncons)
import           Data.Maybe (isJust, fromJust)
import           Data.Monoid ((<>))
import           Data.UUID (UUID, toText, fromText)
import qualified Data.Text as DT
import           Database.SQLite.Simple (SQLData, ToRow, FromRow)
import           Servant (Handler)

-- Task state for abstracting over TaskState
data TaskState = Finished
               | InProgress
               | NotStarted
               | Some deriving (Eq, Enum, Read, Show, Generic)


instance ToJSON TaskState

-- Newtypes preventing careless
type TaskName = DT.Text
type TaskDesc = DT.Text
type TaskStateValue = DT.Text

validTaskStateValues :: [TaskStateValue]
validTaskStateValues = ["Finished", "InProgress", "NotStarted"]

type Partial a = a Maybe
type Complete a = a Identity

data Task = Task { tName  :: TaskName
                 , tDesc  :: TaskDesc
                 , tState :: TaskStateValue
                 } deriving (Eq, Show, Read, Generic)

instance ToJSON Task
instance FromJSON Task

data TaskF f = TaskF { tfName  :: f TaskName
                     , tfDesc  :: f TaskDesc
                     , tfState :: f TaskStateValue
                     }

deriving instance Eq (Complete TaskF)
deriving instance Show (Complete TaskF)
deriving instance Generic (Complete TaskF)

instance ToJSON (Complete TaskF)
instance FromJSON (Complete TaskF)

deriving instance Eq (Partial TaskF)
deriving instance Show (Partial TaskF)
deriving instance Generic (Partial TaskF)

instance ToJSON (Partial TaskF)
instance FromJSON (Partial TaskF)


-- The beefy task class
data TaskFInState (state :: TaskState) f where
    FinishedT :: f TaskName -> f TaskDesc -> TaskFInState 'Finished f
    InProgressT :: f TaskName -> f TaskDesc -> TaskFInState 'InProgress f
    NotStartedT :: f TaskName -> f TaskDesc -> TaskFInState 'NotStarted f

    -- | The case where we don't know what the state actually is
    --   Ex. when we pull a value from the DB, we can't be polymorphic over state with the other constructors
    --   but the database *has* to know what was stored forthe state.
    -- Once we have an UnknownStateT we can write functions that try to translate to what we expect/require and fail otherwise.
    UnknownStateT :: f TaskName -> f TaskDesc -> f TaskStateValue -> TaskFInState state f

    -- | Similar case, but for when we need to fix the state type variable to *something*
    SomeStateT :: f TaskName -> f TaskDesc -> f TaskStateValue -> TaskFInState 'Some f

deriving instance forall (state :: TaskState). Show (Complete (TaskFInState state))
deriving instance forall (state :: TaskState). Show (Partial (TaskFInState state))

instance forall (state :: TaskState). Eq (Complete (TaskFInState state)) where
    v1 == v2 = toTask v1 == toTask v2

-- Aliases for different task states
type FinishedTask = TaskFInState 'Finished
type InProgressTask = TaskFInState 'InProgress
type NotStartedTask = TaskFInState 'NotStarted

----------------
-- Validation --
----------------

type FieldName = DT.Text

data ValidationError = InvalidField FieldName
                     | MissingField FieldName
                     | WrongState DT.Text deriving (Eq, Show, Read, Generic)

instance ToJSON ValidationError

instance Exception ValidationError
instance Exception [ValidationError]

newtype Validated t = Validated { getValidatedObj :: t }

type ValidationCheck t = t -> Maybe ValidationError

class Validatable t where
    -- | Helper method to quickly determine of a type is valid
    isValid :: t -> Bool
    isValid = isRight . validate

    -- | Run all checks on the validated
    validate :: t -> Either [ValidationError] (Validated t)
    validate t = if null errors then Right (Validated t) else Left errors
        where
          checkResults = [check t | check <- validationChecks]
          errors = [fromJust e | e <- checkResults, isJust e]

    -- | List of validation checks to run on the type (any of which could produce an error)
    validationChecks :: [ValidationCheck t]

-- | Utility function for converting a TaskFInState with some state into a regular Task
toTask :: forall (state :: TaskState). Complete (TaskFInState state) -> Task
toTask (NotStartedT (Identity name) (Identity desc)) = Task name desc "NotStarted"
toTask (InProgressT (Identity name) (Identity desc)) = Task name desc "InProgress"
toTask (FinishedT (Identity name) (Identity desc)) = Task name desc "Finished"
toTask (UnknownStateT (Identity name) (Identity desc) (Identity state)) = Task name desc state
toTask (SomeStateT (Identity name) (Identity desc) (Identity state)) = Task name desc state

toTaskFromF :: Complete TaskF -> Task
toTaskFromF (TaskF (Identity name) (Identity desc) (Identity stateValue)) = Task name desc stateValue

taskNameField :: FieldName
taskNameField = "name"

taskDescField :: FieldName
taskDescField = "description"

-- | Helper function to access task name for fully specified task
fsTaskName :: forall (state :: TaskState). Complete (TaskFInState state) -> DT.Text
fsTaskName (FinishedT (Identity name) _) = DT.strip name
fsTaskName (InProgressT (Identity name) _) = DT.strip name
fsTaskName (NotStartedT (Identity name) _) = DT.strip name
fsTaskName (UnknownStateT (Identity name) _ _) = DT.strip name
fsTaskName (SomeStateT (Identity name) _ _) = DT.strip name

-- | Helper function to access task desc for fully specified task
fsTaskDesc :: forall (state :: TaskState). Complete (TaskFInState state) -> DT.Text
fsTaskDesc (FinishedT _ (Identity desc)) = DT.strip desc
fsTaskDesc (InProgressT _ (Identity desc)) = DT.strip desc
fsTaskDesc (NotStartedT _ (Identity desc)) = DT.strip desc
fsTaskDesc (UnknownStateT _ (Identity desc) _) = DT.strip desc
fsTaskDesc (SomeStateT _ (Identity desc) _) = DT.strip desc

showState :: forall (state :: TaskState) (f :: Type -> Type). TaskFInState state f -> String
showState (FinishedT _ _) = "Finished"
showState (InProgressT _ _) = "InProgress"
showState (NotStartedT _ _) = "NotStarted"


instance Validatable (Complete (TaskFInState state)) where
    validationChecks = [checkName, checkDescription]
        where
          checkName :: (Complete (TaskFInState state)) -> Maybe ValidationError
          checkName t = if DT.null (fsTaskName t) then Just (InvalidField taskNameField) else Nothing

          checkDescription :: (Complete (TaskFInState state)) -> Maybe ValidationError
          checkDescription t = if DT.null (fsTaskDesc t) then Just (InvalidField taskDescField) else Nothing

pTaskName :: Partial (TaskFInState state) -> Maybe DT.Text
pTaskName (FinishedT name _) = DT.strip <$> name
pTaskName (InProgressT name _) = DT.strip <$> name
pTaskName (NotStartedT name _) = DT.strip <$> name

pTaskDesc :: forall (state :: TaskState). Partial (TaskFInState state) -> Maybe DT.Text
pTaskDesc (FinishedT _ desc) = DT.strip <$> desc
pTaskDesc (InProgressT _ desc) = DT.strip <$> desc
pTaskDesc (NotStartedT _ desc) = DT.strip <$> desc

nonEmptyIfPresent :: FieldName -> DT.Text -> Maybe ValidationError
nonEmptyIfPresent fieldname v = if DT.null v then Just (InvalidField fieldname) else Nothing

enumStrIfPresent :: [DT.Text] -> FieldName -> DT.Text -> Maybe ValidationError
enumStrIfPresent validValues fieldname v = if not (elem v validValues) then Just (InvalidField fieldname) else Nothing

instance Validatable (Partial (TaskFInState state)) where
    validationChecks = [checkName, checkDescription]
        where
          checkName :: Partial (TaskFInState state) -> Maybe ValidationError
          checkName = maybe Nothing (nonEmptyIfPresent taskNameField) . pTaskName

          checkDescription :: Partial (TaskFInState state) -> Maybe ValidationError
          checkDescription = maybe Nothing (nonEmptyIfPresent taskDescField) . pTaskDesc

instance Validatable (Partial TaskF) where
    validationChecks = [nonEmptyName, nonEmptyDesc, validState]
        where
          nonEmptyName :: Partial TaskF -> Maybe ValidationError
          nonEmptyName = maybe Nothing (nonEmptyIfPresent taskNameField) . tfName

          nonEmptyDesc :: Partial TaskF -> Maybe ValidationError
          nonEmptyDesc = maybe Nothing (nonEmptyIfPresent taskDescField) . tfDesc

          validState :: Partial TaskF -> Maybe ValidationError
          validState = maybe Nothing (enumStrIfPresent validTaskStateValues taskDescField) . tfState

instance Validatable Task where
    validationChecks = [nonEmptyName, nonEmptyDesc, validState]
        where
          nonEmptyName :: Task -> Maybe ValidationError
          nonEmptyName = maybe Nothing (nonEmptyIfPresent taskNameField) . Just . tName

          nonEmptyDesc :: Task -> Maybe ValidationError
          nonEmptyDesc = maybe Nothing (nonEmptyIfPresent taskDescField) . Just . tDesc

          validState :: Task -> Maybe ValidationError
          validState = maybe Nothing (enumStrIfPresent validTaskStateValues taskDescField) . Just . tState

----------------
-- Components --
----------------

class Component c where
    start  :: c -> IO ()
    stop   :: c -> IO ()

class Component c => Constructable c cfg err where
    construct :: cfg -> IO (Either err c)

data WithUUID a = WUUID UUID a deriving (Eq, Show, Read)

instance Functor WithUUID where
    fmap f (WUUID uuid a) = WUUID uuid (f a)

instance ToJSON a => ToJSON (WithUUID a) where
    toJSON (WUUID uuid obj) = case toJSON obj of
                                obj@(Object map) -> Object $ insert "uuid" (String uuidTxt) map
                                v -> object ["uuid" .= uuid, "data" .= v]
        where
          uuidTxt = toText uuid

-- | A typeclass consisting of types that can produce an ID usable by a SQL library to represent itself.
-- for example, if SQL library a can take `Int` for numeric IDs as well as `String`s for UUIDs, and id is an abstraction over both forms of ID,
-- then `getRowIDValue id` will produce the appropriate SQLValue for use in a query using the specified SQL library
class HasRowIDValue id sqlvalue where
    getRowIDValue :: id -> sqlvalue

-- | Holds a database version (expected to be a monotonically increasing number)
newtype SQLMigrationVersion = SQLMigrationVersion { getMigrationVersion :: Int } deriving (Eq, Show, Read, Ord, Num)

-- | Holds a SQL Query
newtype SQLMigrationQuery = SQLMigrationQuery { getMigrationQuery :: DT.Text } deriving (Eq, Show, Read)

-- | Specifies a `SQLMigrationVersion` that is the source of a migration path
type FromSQLMigrationVersion = SQLMigrationVersion

-- | Specifies a `SQLMigrationVersion` that is the target of a migration path
type ToSQLMigrationVersion = SQLMigrationVersion

data SQLMigration = SQLMigration
    { smFrom  :: FromSQLMigrationVersion
    -- ^ The starting migration version
    , smTo    :: ToSQLMigrationVersion
    -- ^ The ending migration version
    , smQuery :: SQLMigrationQuery
    -- ^ Query to execute to perform the migration (also responsible)
    } deriving (Eq, Show)

instance Ord SQLMigration where
    compare l r = compare (smFrom l) (smFrom r)

data MigrationError = NoMigrationPath -- ^ A path between the migrations could not be found
                    | MigrationQueryFailed FromSQLMigrationVersion ToSQLMigrationVersion DT.Text -- ^ An individual migration query failed
                    | VersionFetchFailed DT.Text -- ^ When we've failed to get the current version
                    | UnexpectedMigrationError DT.Text
                      deriving (Eq, Show)

instance Exception MigrationError

class HasMigratableDB store where
    -- | Retreive the desired version, this is normally just statically set @ compile time
    --   The store isn't strictly necessary but just in case we decide to define the desired version in the database or config or elsewhere
    desiredVersion :: store -> IO SQLMigrationVersion

    -- | A list of available migrations that will be used by `migrateTo` to create a path from current (via `getCurrentVersion`) to `desiredVersion`
    availableMigrations :: store -> IO [SQLMigration]

    -- | Retrieve the current version of the database
    getCurrentVersion :: store -> IO (Either MigrationError SQLMigrationVersion)

    -- | Perform migrations to get to the current version
    migrate :: store -> IO (Either MigrationError ())
    migrate store = desiredVersion store >>= migrateTo store

    -- | Finds and executes a path to the requested ToSQLMigration from
    --   Currently when looking through `availableMigrations`, a monotonically increasing version number is assumed,
    --   This means paths are made from version to version generally in one version increments (1 --[migrateTo]--> 2 --[migrateTo]-> 3)
    migrateTo :: store -> ToSQLMigrationVersion -> IO (Either MigrationError ())

data EntityStoreError = NoSuchEntityES UUID DT.Text
                      | UnexpectedErrorES DT.Text
                      | DisconnectedES DT.Text
                      | ConnectionFailureES DT.Text
                      | UnsupportedOperationES DT.Text
                        deriving (Eq, Show, Read)

instance Exception EntityStoreError

newtype TableName entity = TN { getTableName :: DT.Text } deriving (Eq, Show, Read)
newtype SQLColumnNames entity = SQLCN { getColumnNames :: [DT.Text] } deriving (Eq, Show, Read)
type SQLColumnName = DT.Text

class ToRow entity => SQLInsertable entity where
    tableName    :: TableName entity
    columnNames  :: SQLColumnNames entity

-- | Alias for the kind of types with a functor applied to one or more fields (this is be F bounded polymorphism, I think)
--   ex. data T f = T { name :: f DT.Text }, where f might be a type like `Maybe a` or `Identity a`
type FBounded = (Type -> Type) -> Type

class SQLUpdatable e where
    updateColumns :: e -> SQLColumnNames e
    updateColumns = SQLCN . map fst . updateColumnsAndValues

    updateValues  :: e -> [SQLData]
    updateValues = map snd . updateColumnsAndValues

    updateColumnsAndValues :: e -> [(SQLColumnName, SQLData)]
    updateColumnsAndValues e = resolveMaybes $ removeFailedGetters $ applyToE $ updateColumnGetters e
        where
           resolveMaybes = map (second fromJust)
           removeFailedGetters = filter (isJust . snd)
           applyToE = map (second (\fn -> fn e))

    updateColumnGetters :: e -> [(SQLColumnName, e -> Maybe SQLData)]

data DeletionMode e = Soft
                    | Hard deriving (Eq, Show, Read)

class SQLDeletable entity where
    deletionMode :: DeletionMode entity

-- | Generalized typeclass for entity storage.
class SQLEntityStore store where
    -- | Create an entity
    create :: forall entity.
              ( SQLInsertable entity
              , SQLInsertable (WithUUID entity)
              , FromRow (WithUUID entity)
              )
             => store
                 -> Validated entity
                 -> IO (Either EntityStoreError (WithUUID entity))

    -- | Get an entity by ID
    getByUUID :: forall entity.
              ( SQLInsertable entity
              , FromRow (WithUUID entity)
              )
              => store
                 -> UUID
                 -> IO (Either EntityStoreError (WithUUID entity))

    -- | Update an existing entity by ID
    updateByUUID :: forall (entity :: FBounded).
                  ( SQLInsertable (Complete entity)
                  , SQLUpdatable (Partial entity)
                  , FromRow (Complete entity)
                  )
                 => store
                     -> UUID
                     -> Validated (Partial entity)
                     -> IO (Either EntityStoreError (WithUUID (Complete entity)))

    -- | Delete an entity by ID
    deleteByUUID :: forall entity.
                  ( SQLInsertable entity
                  , SQLDeletable entity
                  , FromRow entity
                  )
                 => store
                     -> UUID
                     -> IO (Either EntityStoreError entity)

    -- | Get a listing of all entities
    list :: forall entity.
            ( SQLInsertable entity
            , FromRow entity
            )
           => store
               -> IO (Either EntityStoreError [entity])

-- | Our application state
data AppState = forall estore. SQLEntityStore estore =>
                AppState { appConfig   :: Complete AppConfig
                         , entityStore :: estore
                         }

-- | Our custom application handler monad for use with servant
type AppHandler = ReaderT AppState Handler

-- | Natural transformation for custom servant monad
appToServantHandler :: AppState -> AppHandler a -> Handler a
appToServantHandler state appM = runReaderT appM state

data Entity where
    TaskE :: TaskName -> Entity
