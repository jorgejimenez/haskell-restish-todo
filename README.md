# haskell-restish-todo #

REST-ish TODO application written in [Haskell](https://www.haskell.org/), to go with [a series of blog posts called "REST-ish services in Haskell"](https://vadosware.io/post/rest-ish-services-in-haskell-part-1/) @ [vadosware.io](https://vadosware.io).

This repo contains code that is (hopefully) helpful to those wishing to get started build web services with Haskell, but aren't able to find decent examples for from-scratch projects. Series in the blog posts correspond to tags in this repo (ex. `part-1`, `part-2`, etc).
